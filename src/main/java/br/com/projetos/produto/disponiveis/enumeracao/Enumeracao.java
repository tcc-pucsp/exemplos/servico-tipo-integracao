package br.com.projetos.produto.disponiveis.enumeracao;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum Enumeracao {

    PRODUTO_PENDENTE(0),
    PRODUTO_ATIVO(1);

    private final Integer status;

    public boolean igual(Integer status) {
        return this.status.equals(status);
    }

}
