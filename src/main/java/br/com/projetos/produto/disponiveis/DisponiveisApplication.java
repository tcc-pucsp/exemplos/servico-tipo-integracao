package br.com.projetos.produto.disponiveis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DisponiveisApplication {

    public static void main(String[] args) {
        SpringApplication.run(DisponiveisApplication.class, args);
    }

}
