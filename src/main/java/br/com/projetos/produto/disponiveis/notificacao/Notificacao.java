package br.com.projetos.produto.disponiveis.notificacao;

import br.com.projetos.produto.disponiveis.dto.response.RespostaDTO;
import br.com.projetos.produto.disponiveis.excessao.ProdutoException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import reactor.core.publisher.Mono;

@RestControllerAdvice
public class Notificacao {

    @ExceptionHandler(ProdutoException.class)
    public ResponseEntity<Mono<RespostaDTO>> produtoExcessao(ProdutoException e) {
        return ResponseEntity.status(e.getHttpStatus()).body(e.getRespostaDTO());
    }
}
