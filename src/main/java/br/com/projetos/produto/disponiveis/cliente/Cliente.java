package br.com.projetos.produto.disponiveis.cliente;

import br.com.projetos.produto.disponiveis.dto.response.ProdutoDTO;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface Cliente {

    Flux<ProdutoDTO> obterTodosProdutos();

    Mono<ProdutoDTO> obterProdutoPorId(Long id);

}
