package br.com.projetos.produto.disponiveis.cliente;

import br.com.projetos.produto.disponiveis.dto.response.ProdutoDTO;
import br.com.projetos.produto.disponiveis.excessao.ProdutoException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class ClienteConcreto implements Cliente {

    @Value("${produto.url}")
    private String produtoUrl;

    private final WebClient webClient;

    public ClienteConcreto(WebClient webClient) {
        this.webClient = webClient;
    }

    @Override
    public Flux<ProdutoDTO> obterTodosProdutos() {

        return webClient
                .get()
                .uri(produtoUrl)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, this::responseError)
                .bodyToFlux(ProdutoDTO.class);
    }

    @Override
    public Mono<ProdutoDTO> obterProdutoPorId(Long id) {

        return webClient
                .get()
                .uri(produtoUrl.concat("/{id}"), id)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, this::responseError)
                .bodyToMono(ProdutoDTO.class);

    }

    private Mono<? extends Throwable> responseError(ClientResponse clientResponse) {
        throw new ProdutoException(clientResponse);
    }

}