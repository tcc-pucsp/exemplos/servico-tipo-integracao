package br.com.projetos.produto.disponiveis.excessao;

import br.com.projetos.produto.disponiveis.dto.response.RespostaDTO;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.ClientResponse;
import reactor.core.publisher.Mono;

@Getter
public class ProdutoException extends RuntimeException {

    private final transient Mono<RespostaDTO> respostaDTO;

    private final transient HttpStatus httpStatus;

    public ProdutoException(ClientResponse clientResponse) {
        this.respostaDTO = clientResponse.bodyToMono(RespostaDTO.class);
        this.httpStatus = clientResponse.statusCode();
    }
}
