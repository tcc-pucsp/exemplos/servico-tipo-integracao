package br.com.projetos.produto.disponiveis.dto.response;

import lombok.Data;

@Data
public class RespostaDTO {

    private Integer codigo;

    private String descricao;

    private String mensagem;

}
