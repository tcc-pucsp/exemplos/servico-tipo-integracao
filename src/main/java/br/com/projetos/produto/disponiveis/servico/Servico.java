package br.com.projetos.produto.disponiveis.servico;

import br.com.projetos.produto.disponiveis.cliente.Cliente;
import br.com.projetos.produto.disponiveis.dto.response.ProdutoDTO;
import br.com.projetos.produto.disponiveis.enumeracao.Enumeracao;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class Servico {

    private final Cliente cliente;

    public Servico(Cliente cliente) {
        this.cliente = cliente;
    }

    public Flux<ProdutoDTO> obterProdutosComStatusIgualAtivo() {
        return cliente
                .obterTodosProdutos()
                .filter(produto -> Enumeracao.PRODUTO_ATIVO.igual(produto.getStatus()));
    }

    public Mono<ProdutoDTO> obterProdutoPorId(Long id) {
        return cliente.obterProdutoPorId(id);
    }

}
