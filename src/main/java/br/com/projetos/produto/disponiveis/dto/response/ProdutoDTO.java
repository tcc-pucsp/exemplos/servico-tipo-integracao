package br.com.projetos.produto.disponiveis.dto.response;

import lombok.Data;

@Data
public class ProdutoDTO {

    private String nome;

    private Integer quantidadeDisponivel;

    private Integer status;

    private Integer desconto;

    private Float valor;

}
