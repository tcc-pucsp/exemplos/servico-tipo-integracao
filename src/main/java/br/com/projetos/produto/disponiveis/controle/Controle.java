package br.com.projetos.produto.disponiveis.controle;

import br.com.projetos.produto.disponiveis.dto.response.ProdutoDTO;
import br.com.projetos.produto.disponiveis.servico.Servico;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class Controle {

    private final Servico servico;

    public Controle(Servico servico) {
        this.servico = servico;
    }

    @GetMapping
    public Flux<ProdutoDTO> obterProdutosComStatusIgualAtivo() {
        return servico.obterProdutosComStatusIgualAtivo();
    }

    @GetMapping("{id}")
    public Mono<ProdutoDTO> obterPorId(@PathVariable("id") Long id) {
        return servico.obterProdutoPorId(id);
    }

}
