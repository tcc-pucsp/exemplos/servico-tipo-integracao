package br.com.projetos.produto.disponiveis.fabrica;

import br.com.projetos.produto.disponiveis.dto.response.ProdutoDTO;
import br.com.projetos.produto.disponiveis.enumeracao.Enumeracao;

public interface ProdutoFabrica {

    static ProdutoDTO criar(Enumeracao enumeracao){

        ProdutoDTO produtoDTO = new ProdutoDTO();

        produtoDTO.setDesconto(34);
        produtoDTO.setNome("MARTELO");
        produtoDTO.setStatus(enumeracao.getStatus());
        produtoDTO.setQuantidadeDisponivel(56);
        produtoDTO.setValor(3723f);

        return produtoDTO;
    }
}
