package br.com.projetos.produto.disponiveis.fabrica;

import br.com.projetos.produto.disponiveis.dto.response.RespostaDTO;

public interface RespostaFabrica {

    static RespostaDTO criar() {

        RespostaDTO respostaDTO = new RespostaDTO();

        respostaDTO.setCodigo(1001);
        respostaDTO.setDescricao("Produto não foi encontrado");
        respostaDTO.setMensagem("produto com id '4'");

        return respostaDTO;

    }
}
