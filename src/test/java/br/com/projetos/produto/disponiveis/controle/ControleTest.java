package br.com.projetos.produto.disponiveis.controle;

import br.com.projetos.produto.disponiveis.dto.response.ProdutoDTO;
import br.com.projetos.produto.disponiveis.dto.response.RespostaDTO;
import br.com.projetos.produto.disponiveis.enumeracao.Enumeracao;
import br.com.projetos.produto.disponiveis.fabrica.ProdutoFabrica;
import br.com.projetos.produto.disponiveis.fabrica.RespostaFabrica;
import br.com.projetos.produto.disponiveis.ferramentas.GsonUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.matchers.Times;
import org.mockserver.model.Header;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.Arrays;

import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
public class ControleTest {

    @Value("${produto.port}")
    private int port;

    private WebTestClient client;

    private ClientAndServer clientAndServer;

    @Autowired
    private ApplicationContext context;

    @Before
    public void setup() {

        clientAndServer = new ClientAndServer(port);

        client = WebTestClient
                .bindToApplicationContext(this.context)
                .configureClient()
                .build();

    }

    @Test
    public void obterTodosProdutosComStatusAtivo() {

        final ProdutoDTO[] produtos = {
                ProdutoFabrica.criar(Enumeracao.PRODUTO_ATIVO),
                ProdutoFabrica.criar(Enumeracao.PRODUTO_PENDENTE)
        };

        clientAndServer
                .when(request().withMethod("GET"), Times.exactly(1))
                .respond(response()
                        .withHeader(new Header("Content-Type", MediaType.APPLICATION_JSON_UTF8_VALUE))
                        .withBody(GsonUtils.objectToString(produtos))
                        .withStatusCode(HttpStatus.OK.value()));

        client
                .get()
                .uri("")
                .exchange()
                .expectStatus().isOk()
                .expectBody(ProdutoDTO[].class)
                .isEqualTo(Arrays
                        .stream(produtos)
                        .filter(produto -> Enumeracao.PRODUTO_ATIVO.igual(produto.getStatus()))
                        .toArray(ProdutoDTO[]::new)
                );

    }

    @Test
    public void obterTodosProdutosComStatusAtivoPoremComRetorno4xx() {

        final RespostaDTO respostaDTO = RespostaFabrica.criar();

        clientAndServer
                .when(request().withMethod("GET"), Times.exactly(1))
                .respond(response()
                        .withHeader(new Header("Content-Type", MediaType.APPLICATION_JSON_UTF8_VALUE))
                        .withBody(GsonUtils.objectToString(respostaDTO))
                        .withStatusCode(HttpStatus.FORBIDDEN.value()));

        client
                .get()
                .uri("")
                .exchange()
                .expectStatus().isForbidden()
                .expectBody(RespostaDTO.class)
                .isEqualTo(respostaDTO);

    }

    @Test
    public void obterProdutoPorId() {

        final ProdutoDTO produtoDTO = ProdutoFabrica.criar(Enumeracao.PRODUTO_ATIVO);

        final Long id = 1L;

        clientAndServer
                .when(request()
                        .withMethod("GET")
                        .withPath("/{id}"
                                .replace("{id}", id.toString())))
                .respond(response()
                        .withHeader(new Header("Content-Type", MediaType.APPLICATION_JSON_UTF8_VALUE))
                        .withBody(GsonUtils.objectToString(produtoDTO))
                        .withStatusCode(HttpStatus.OK.value()));

        client
                .get()
                .uri("/{id}", id)
                .exchange()
                .expectStatus().isOk()
                .expectBody(ProdutoDTO.class)
                .isEqualTo(produtoDTO);

    }

    @Test
    public void obterProdutoPorIdInexistente() {

        final Long id = 4L;

        final RespostaDTO respostaDTO = RespostaFabrica.criar();

        clientAndServer
                .when(request()
                        .withMethod("GET")
                        .withPath("/{id}"
                                .replace("{id}", id.toString())))
                .respond(response()
                        .withHeader(new Header("Content-Type", MediaType.APPLICATION_JSON_UTF8_VALUE))
                        .withBody(GsonUtils.objectToString(respostaDTO))
                        .withStatusCode(HttpStatus.NOT_FOUND.value()));

        client
                .get()
                .uri("/{id}", id)
                .exchange()
                .expectStatus().isNotFound()
                .expectBody(RespostaDTO.class)
                .isEqualTo(respostaDTO);

    }

    @After
    public void stopMockServer() {
        clientAndServer.stop();
    }

}
